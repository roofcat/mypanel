# encoding:utf-8
#!/usr/bin/env python

import webapp2

from app.utils.oauthutils import decorator
from app.orgunits import groups
from app.usertools import userdirectory
from app.contacttools import sharedcontact
from app.drive import driveabout
from app.profile import profile
from app.panel import about
from app.emailtools import signature
from app.emailtools import signature_cron


app = webapp2.WSGIApplication([
    # modulo del perfil
    ('/', profile.AuthControl),
    ('/profile', profile.MyProfile),
    ('/profile/edit', profile.EditMyProfile),
    ('/revoke', profile.RevokeOAuth2),
    ('/about', about.MyAbout),
    # modulo shared contacts
    ('/contacttools/mycontacts', sharedcontact.MyContacts),
    ('/contacttools/newcontact', sharedcontact.CreateNewContact),
    ('/contacttools/edit/(.+)', sharedcontact.EditContact),
    # modulo usuarios del dominio
    ('/usertools/users', userdirectory.ShowMyDirectory),
    ('/usertools/user/(?P<user_id>.*)', userdirectory.EditUserDirectory),
    ('/usertools/newuser', userdirectory.CreateNewUser),
    # modulo de firmas
    ('/emailtools/signature', signature.SetupSignatures),
    ('/emailtools/signature/query', signature_cron.CronDbSignatureHandler),
    # ruta de inicio de las tareas cron
    ('/emailtools/signature/cron', signature_cron.RunCronSignatureHandler),
    ('/emailtools/signature/test', signature_cron.TestCron),
    # modulos de grupos y unidades organizativas
    ('/orgunits', groups.MyOrgUnitMenu),
    ('/orgunits/index', groups.MyOrgUnitIndex),
    ('/orgunits/list', groups.MyOrgUnitList),
    ('/orgunits/new', groups.CreateNewOrgUnit),
    ('/orgunits/del', groups.DeleteOrgUnit),
    ('/orgunits/test', groups.Test),
    ('/orgunits/grouplist', groups.MyGroupsList),
    # modulo de informacion de google drive del usuario
    ('/drive/about', driveabout.DriveAboutHandler),
    (decorator.callback_path, decorator.callback_handler()),
], debug=True)

