# encoding:utf-8
#!/usr/bin/env python

from oauth2client.client import AccessTokenRefreshError

import gdata.contacts.client
import gdata.contacts.data
import gdata.data


import webapp2


from app.utils.oauthutils import decorator
from app.utils.oauthutils import user_info_service
from app.utils.configs import JINJA_ENVIRONMENT
from app.utils import tokenutils


class MyContacts(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)

            auth_token = tokenutils.OAuth2TokenFromCredentials(
                decorator.credentials)
            gd_contact = gdata.contacts.client.ContactsClient(
                domain=data['hd'])
            auth_token.authorize(gd_contact)

            feed_url = gd_contact.GetFeedUri(
                contact_list=data['hd'], projection='full')
            feed = gd_contact.get_feed(
                uri=feed_url, desired_class=gdata.contacts.data.ContactsFeed)

            context = {'data': data, 'feed': feed, }
            template = JINJA_ENVIRONMENT.get_template(
                '/contacts/my_contacts.html')
            self.response.write(template.render(context))
        except AccessTokenRefreshError:
            self.redirect('/')


class CreateNewContact(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            context = {
                'data': data,
            }
            template = JINJA_ENVIRONMENT.get_template(
                '/contacts/newcontact.html')
            self.response.write(template.render(context))
        except:
            self.response.out.write("Error! you don't have permission")

    @decorator.oauth_required
    def post(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            auth_token = tokenutils.OAuth2TokenFromCredentials(
                decorator.credentials)
            gd_contact = gdata.contacts.client.ContactsClient(
                domain=data['hd'])
            auth_token.authorize(gd_contact)

            # set uri de contactos compartidos para el dominio
            feed_url = gd_contact.GetFeedUri(
                contact_list=data['hd'], projection='full')

            # inicializando el objeto del nuevo contacto
            new_contact = gdata.contacts.data.ContactEntry()

            new_contact.name = gdata.data.Name(
                given_name=gdata.data.GivenName(
                    text=self.request.get('givenName')),
                family_name=gdata.data.FamilyName(
                    text=self.request.get('familyName')),
                full_name=gdata.data.FullName(text=self.request.get('fullName')))

            if self.request.get('email'):
                new_contact.email.append(gdata.data.Email(
                    address=self.request.get('email'),
                    rel=gdata.data.WORK_REL))

            if self.request.get('phone'):
                new_contact.phone_number.append(
                    gdata.data.PhoneNumber(text=self.request.get('phone'),
                                           rel=gdata.data.WORK_REL))

            contact_entry = gd_contact.CreateContact(
                new_contact=new_contact, insert_uri=feed_url)
            if contact_entry:
                template = JINJA_ENVIRONMENT.get_template(
                    "/contacts/newcontact_success.html")
                context = {
                    'data': data,
                    'new_contact': new_contact
                }
                self.response.write(template.render(context))
            else:
                self.response.write("Error")
        except:
            self.response.write(
                "Error, Authorization failed or you haven't permission.")


class EditContact(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self, contact_id):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            try:
                auth_token = tokenutils.OAuth2TokenFromCredentials(
                    decorator.credentials)
                gd_contact = gdata.contacts.client.ContactsClient(
                    domain=data['hd'])
                auth_token.authorize(gd_contact)

                feed_url = gd_contact.GetFeedUri(
                    contact_list=data['hd'], projection='full')
                feed = gd_contact.get_feed(
                    uri=feed_url, desired_class=gdata.contacts.data.ContactsFeed)

                template = JINJA_ENVIRONMENT.get_template(
                    "/contacts/updatecontact.html")
                context = {
                    'data': data,
                    'contact': feed.entry[int(contact_id)],
                }
                self.response.write(template.render(context))
            except:
                self.response.write("Wrong contact ID.")
        except:
            self.response.write("Error, authenticacion failure.")

    @decorator.oauth_required
    def post(self, contact_id):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            try:
                auth_token = tokenutils.OAuth2TokenFromCredentials(
                    decorator.credentials)
                gd_contact = gdata.contacts.client.ContactsClient(
                    domain=data['hd'])
                auth_token.authorize(gd_contact)

                feed_url = gd_contact.GetFeedUri(
                    contact_list=data['hd'], projection='full')
                feed = gd_contact.get_feed(
                    uri=feed_url, desired_class=gdata.contacts.data.ContactsFeed)
                contact = feed.entry[int(contact_id)]
            except:
                self.response.write("Wrong contact ID.")

            if self.request.get('givenName'):
                contact.name.given_name.text = self.request.get('givenName')

            if self.request.get('familyName'):
                contact.name.family_name.text = self.request.get('familyName')

            if self.request.get('fullName'):
                contact.name.full_name.text = self.request.get('fullName')

            emails = self.request.get('email', allow_multiple=True)
            contact.email = []  # limpiar objecto email para actualizar
            if emails:
                for email in emails:
                    if email:
                        contact.email.append(gdata.data.Email(
                            address=email,
                            rel=gdata.data.WORK_REL))

            phones = self.request.get('phone', allow_multiple=True)
            # limpiar objecto phone_number para actualizar
            contact.phone_number = []
            if phones:
                for phone in phones:
                    if phone:
                        contact.phone_number.append(gdata.data.PhoneNumber(
                            text=phone,
                            rel=gdata.data.WORK_REL))

            gd_contact.Update(entry=contact)
            template = JINJA_ENVIRONMENT.get_template(
                "/contacts/updatecontact_success.html")
            context = {
                'data': data,
            }
            self.response.write(template.render(context))
        except:
            self.response.write("Update Shared Contact Error")


class Tests(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self, contact_id):
        http = decorator.http()
        data = user_info_service.userinfo().get().execute(http=http)

        auth_token = tokenutils.OAuth2TokenFromCredentials(
            decorator.credentials)
        gd_contact = gdata.contacts.client.ContactsClient(domain=data['hd'])
        auth_token.authorize(gd_contact)

        feed_url = gd_contact.GetFeedUri(
            kind='profiles', contact_list=data['hd'], projection='full')
        feed = gd_contact.get_feed(
            uri=feed_url, desired_class=gdata.contacts.data.ContactsFeed)
        contact = feed.entry[int(contact_id)]
        my_uri = contact.id.text

        contact_profile = gd_contact.GetProfile(uri=my_uri)
        contact_profile.status.indexed = 'true'
        gd_contact.Update(contact_profile)

        template = JINJA_ENVIRONMENT.get_template("test.html")
        context = {
            'data': data,
            'contact': contact,
        }
        self.response.write(contact_profile.__dict__)
