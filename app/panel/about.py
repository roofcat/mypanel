# encoding:utf-8
#!/usr/bin/env python

import webapp2


from app.utils.oauthutils import decorator
from app.utils.oauthutils import user_info_service
from app.utils.configs import JINJA_ENVIRONMENT


class MyAbout(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            context = {
                'data': data,
            }
            template = JINJA_ENVIRONMENT.get_template('myabout.html')
            self.response.write(template.render(context))
        except:
            self.redirect('/')
