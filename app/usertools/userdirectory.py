# encoding:utf-8
#!/usr/bin/env python

import webapp2


from app.utils.oauthutils import decorator
from app.utils.oauthutils import user_info_service
from app.utils.oauthutils import admin_service
from app.utils.configs import JINJA_ENVIRONMENT


class CreateNewUser(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            context = {
                'data': data,
            }
            template = JINJA_ENVIRONMENT.get_template('/users/newuser.html')
            self.response.write(template.render(context))
        except:
            self.response.out.write("Error! you don't have permission")

    @decorator.oauth_required
    def post(self):
        givenName = self.request.get('givenName')
        familyName = self.request.get('familyName')
        userName = self.request.get('userName')
        domain = self.request.get('domain')
        password1 = self.request.get('password1')
        password2 = self.request.get('password2')

        if not self.request.get('changePass'):
            changePass = False
        else:
            changePass = True

        primaryEmail = userName + '@' + domain
        fullName = givenName + ' ' + familyName

        if givenName and familyName and userName and domain and password1 and password2:
            if password1 == password2:
                password = password1
                new_user_info = {
                    'primaryEmail': primaryEmail,
                    'name': {
                        'givenName': givenName,
                        'familyName': familyName,
                        'fullName': fullName
                    },
                    'password': password,
                    'changePasswordAtNextLogin': changePass,
                }
                http = decorator.http()
                data = user_info_service.userinfo().get().execute(http=http)
                admin_service.users().insert(
                    body=new_user_info).execute(http=http)
                context = {
                    'data': data,
                    'new_user': new_user_info,
                }
                template = JINJA_ENVIRONMENT.get_template(
                    '/users/newuser_success.html')
                self.response.write(template.render(context))
            else:
                self.redirect('/usertools/newuser')
        else:
            self.redirect('/usertools/newuser')


class ShowMyDirectory(webapp2.RequestHandler):

    """
    Clase encargada de mostrar (a un usuario Admin) todos los usuarios del dominio
    """
    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            my_directory = admin_service.users().list(
                customer='my_customer', domain=data['hd']).execute(http=http)
            context = {
                'data': data,
                'my_directory': my_directory,
            }
            template = JINJA_ENVIRONMENT.get_template(
                '/users/my_directory.html')
            self.response.write(template.render(context))
        except:
            self.redirect('/')


class EditUserDirectory(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self, user_id):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            user_data = admin_service.users().get(
                userKey=user_id).execute(http=http)
            context = {
                'data': data,
                'user_data': user_data,
            }
            template = JINJA_ENVIRONMENT.get_template('/users/updateuser.html')
            self.response.write(template.render(context))
        except:
            self.response.out.write(
                "<strong>Error, Wrong parameters!</strong>")

    @decorator.oauth_required
    def post(self, user_id):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            user_data = admin_service.users().get(
                userKey=user_id).execute(http=http)

            primaryEmail = self.request.get('primaryEmail')
            givenName = self.request.get('givenName')
            familyName = self.request.get('familyName')

            # inicio objeto organizations
            orgNames = self.request.get('orgName', allow_multiple=True)
            orgDepartment = self.request.get(
                'orgDepartment', allow_multiple=True)
            organizations = []
            for o in range(len(orgNames)):
                if orgNames[o]:
                    organizations.append(
                        {'name': orgNames[o], 'department': orgDepartment[o]})

            # inicio objeto addresses
            addressName = self.request.get('addressName', allow_multiple=True)
            addressType = self.request.get('addressType', allow_multiple=True)
            addresses = []
            for a in range(len(addressName)):
                if addressName[a]:
                    addresses.append(
                        {'formatted': addressName[a], 'type': addressType[a]})

            # inicio objeto phones
            items = self.request.get('phonesValue', allow_multiple=True)
            types = self.request.get('phonesType', allow_multiple=True)
            phones = []
            for p in range(len(items)):
                if items[p]:
                    phones.append({'value': items[p], 'type': types[p]})

            # inicio objeto body
            body = {
                'name': {
                    'givenName': givenName,
                    'familyName': familyName
                },
                'organizations': organizations,
                'phones': phones,
                'addresses': addresses,
            }
            if user_id == primaryEmail:
                admin_service.users().update(
                    userKey=primaryEmail, body=body).execute(http=http)
                context = {
                    'data': data,
                    'user_data': user_data,
                }
                template = JINJA_ENVIRONMENT.get_template(
                    '/users/updateuser_success.html')
                self.response.write(template.render(context))
            else:
                self.response.out.write(
                    "<strong>Error, Wrong parameters!</strong>")
        except:
            self.response.out.write(
                "<strong>Error, Wrong parameters!</strong>")
