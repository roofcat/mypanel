#encoding:utf-8
#!/usr/bin/env python

import json
import urllib2


def get_me_from_gal(hd, email, token):
	"""
	Retorna los datos del contacto almacenados en la gal del directorio
	"""
	return json.load(
		urllib2.urlopen('https://www.google.com/m8/feeds/gal/%s/full?alt=json&q=%s&access_token=%s'%(hd, email, token)))


def get_my_directory(hd, token):
	return json.load(
		urllib2.urlopen('https://www.google.com/m8/feeds/gal/%s/full?alt=json&access_token=%s'%(hd, token)))


def revoke_access_token(token):
	"""
	Permite al usuario eliminar permiso de acceso a la aplicación
	"""
	return urllib2.urlopen('https://accounts.google.com/o/oauth2/revoke?token=%s'%token)