#encoding:utf-8
#!/usr/bin/env python

from gdata.gauth import OAuth2Token


import httplib2


class OAuth2TokenFromCredentials(OAuth2Token):
	def __init__(self, credentials):
		self.credentials = credentials
		super(OAuth2TokenFromCredentials, self).__init__(None, None, None, None)
		self.UpdateFromCredentials()
	
	def UpdateFromCredentials(self):
		self.client_id = self.credentials.client_id
		self.client_secret = self.credentials.client_secret
		self.user_agent = self.credentials.user_agent
		self.token_uri = self.credentials.token_uri
		self.access_token = self.credentials.access_token
		self.refresh_token = self.credentials.refresh_token
		self.token_expiry = self.credentials.token_expiry
		self._invalid = self.credentials.invalid
	
	def generate_authorize_url(self, *args, **kwargs): raise NotImplementedError

	def get_access_token(self, *args, **kwargs): raise NotImplementedError

	def revoke(self, *args, **kwargs): raise NotImplementedError
	
	def _extract_tokens(self, *args, **kwargs): raise NotImplementedError
	
	def _refresh(self, unused_request):
		self.credentials._refresh(httplib2.Http().request())
		self.UpdateFromCredentials()