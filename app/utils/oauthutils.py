# encoding:utf-8
#!/usr/bin/env python

from oauth2client.appengine import OAuth2Decorator
from apiclient.discovery import build
from google.appengine.api import memcache


import httplib2


http = httplib2.Http(memcache)
plus_service = build('plus', 'v1', http=http)
user_info_service = build('oauth2', 'v2', http=http)
admin_service = build('admin', 'directory_v1', http=http)
drive_service = build('drive', 'v2', http=http)


CLIENT_ID = '121352951201-h6btvq6ar1rumln8a078l7m6n2r1fdi7.apps.googleusercontent.com'
CLIENT_SECRET = 'hqNkEB99Qdnxu-1rfSqGRAhU'


decorator = OAuth2Decorator(
    client_id=CLIENT_ID,
    client_secret=CLIENT_SECRET,
    access_type='offline',
    approval_prompt='force',
    scope='https://www.googleapis.com/auth/plus.me ' +
    'https://www.googleapis.com/auth/userinfo.profile ' +
    'https://www.googleapis.com/auth/userinfo.email ' +
    'https://www.googleapis.com/auth/admin.directory.orgunit ' +
    'https://www.googleapis.com/auth/admin.directory.user ' +
    'https://www.googleapis.com/auth/admin.directory.group ' +
    'https://www.google.com/m8/feeds ' +
    'https://www.googleapis.com/auth/contacts ' +
    'https://docs.google.com/feeds/ ' +
    'https://sites.google.com/feeds/ ' +
    'https://spreadsheets.google.com/feeds/ ' +
    'https://www.googleapis.com/auth/calendar ' +
    'https://www.googleapis.com/auth/drive ' +
    'https://apps-apis.google.com/a/feeds/emailsettings/2.0/ ' +
    'https://www.googleapis.com/auth/taskqueue ' +
    'https://www.googleapis.com/auth/tasks ' +
    'https://www.googleapis.com/auth/drive ',
)


# mensaje de error de autenticación cuando el usuario no tiene cuenta con
# privilegios de administrador
OAUTH_ERROR = """<div class="alert alert-warning">
        <strong>Authentication Error!</strong> You don't have Admin account.
    </div>
"""
