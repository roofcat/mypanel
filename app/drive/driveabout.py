# encoding:utf-8
#!/usr/bin/env python

import webapp2
import json


from app.utils.oauthutils import decorator
from app.utils.oauthutils import drive_service


class DriveAboutHandler(webapp2.RequestHandler):

    """ Información de la unidad del Drive del usuario 
    Descripción de atributos:
    - quotaBytesUsedInTrash: "318395"
    - quotaBytesUsedAggregate: "493959566"
    - quotaBytesTotal: "32212254720"
    - quotaBytesUsed: "385731869"
    - quotaBytesByService: [{
            bytesUsed: "385731869",
            serviceName: "DRIVE"},
            {bytesUsed: "108227697",
            serviceName: "GMAIL"},
            {bytesUsed: "0",
            serviceName: "PHOTOS"}]
    """

    @decorator.oauth_required
    def get(self):
        http = decorator.http()
        drive_about = drive_service.about().get().execute(http=http)
        self.response.write(json.dumps(drive_about))
