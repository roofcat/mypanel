# encoding:utf-8
#!/usr/bin/env python

import webapp2
import json
import httplib2


from gdata.apps.emailsettings.client import EmailSettingsClient
from apiclient.discovery import build


from app.utils.oauthutils import decorator
from app.utils.oauthutils import user_info_service
from app.utils.configs import JINJA_ENVIRONMENT
from app.utils import tokenutils
from app.mailing.mail import MailingTool
from model.signature_model import SignatureCronTask
from model.user_model import UserModel


"""
class TestCron(webapp2.RequestHandler):

    def get(self):
        my_cron = SignatureCronTask()
        data = my_cron.list_signatures()
        self.response.write(data)
"""


class CronDbSignatureHandler(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            my_cron = SignatureCronTask()
            query = my_cron.find_signatures(data['email'])
            self.response.write(json.dumps(len(query)))
        except:
            pass

    @decorator.oauth_required
    def delete(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            my_cron = SignatureCronTask()
            my_cron.delete_signatures(data['email'])
            self.response.write(json.dumps(True))
        except:
            self.response.write(json.dumps(False))


class RunCronSignatureHandler(webapp2.RequestHandler):

    def get(self):
        my_cron = SignatureCronTask()
        user_model = UserModel()
        my_mail = MailingTool()

        signatures_list = my_cron.list_signatures()

        for my_list in signatures_list:
            current_credential = user_model.get_credential(my_list.user_id)
            http = httplib2.Http()
            http = current_credential.authorize(http)
            admin_service = build('admin', 'directory_v1', http=http)
            userinfo = build('oauth2', 'v2', http=http)
            data = userinfo.userinfo().get().execute()
            my_directory = admin_service.users().list(
                customer='my_customer', domain=my_list.domain).execute()
            auth_token = tokenutils.OAuth2TokenFromCredentials(
                current_credential)
            emailsettings = EmailSettingsClient(domain=my_list.domain)
            auth_token.authorize(emailsettings)

            signatureSchema = my_list.signature

            for d in my_directory['users']:
                firstName = d['name']['givenName']
                lastName = d['name']['familyName']
                primaryEmail = d['primaryEmail']
                company = ''
                department = ''
                occupation = ''
                workPhone = ''
                homePhone = ''
                mobilePhone = ''
                workAddress = ''
                homeAddress = ''

                try:
                    if d['organizations']:
                        for o in d['organizations']:
                            occupation = o['title'] if o['title'] else ''
                            department = o['department'] if o[
                                'department'] else ''
                            company = o['name'] if o['name'] else ''
                except:
                    pass

                try:
                    if d['phones']:
                        for p in d['phones']:
                            if p['type'] == 'work':
                                workPhone = p['value']
                            elif p['type'] == 'mobile':
                                mobilePhone = p['value']
                            elif p['type'] == 'home':
                                homePhone = p['value']
                except:
                    pass

                try:
                    if d['addresses']:
                        for a in d['addresses']:
                            if a['type'] == 'work':
                                workAddress = a['formatted']
                            elif a['type'] == 'home':
                                homeAddress = a['formatted']
                except:
                    pass

                try:
                    signature = signatureSchema.format(
                        firstName=firstName, lastName=lastName, primaryEmail=primaryEmail,
                        company=company, occupation=occupation, department=department,
                        workPhone=workPhone, homePhone=homePhone, mobilePhone=mobilePhone,
                        workAddress=workAddress, homeAddress=homeAddress
                    )

                    emailsettings.UpdateSignature(
                        username=primaryEmail, signature=signature)
                except Exception, e:
                    my_mail.SendEmail(
                        to="crojas@azurian.com",
                        subject="Error al actualizar firma Cron",
                        html=e
                    )

            my_mail.SendEmail(
                to="crojas@azurian.com",
                subject="Cron en AppEngine",
                html="Hola, " + data['name'] +
                        " Se ha ejecutado tu tarea cron con la siguiente firma: " +
                        signatureSchema +
                        " Saludos! ",
            )
