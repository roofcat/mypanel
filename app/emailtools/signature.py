# encoding:utf-8
#!/usr/bin/env python

import webapp2


from gdata.apps.emailsettings.client import EmailSettingsClient


from app.utils.oauthutils import decorator
from app.utils.oauthutils import user_info_service
from app.utils.oauthutils import admin_service
from app.utils.configs import JINJA_ENVIRONMENT
from app.utils import tokenutils
from app.mailing.mail import MailingTool
from model.signature_model import SignatureCronTask


class SetupSignatures(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            template = JINJA_ENVIRONMENT.get_template(
                '/signatures/signature.html')
            context = {
                'data': data,
            }
            self.response.write(template.render(context))
        except:
            self.response.out.write(
                "<strong>Error, solicitud no completada</strong>")

    @decorator.oauth_required
    def post(self):
        try:
            my_mail = MailingTool()
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(
                http=http)
            my_directory = admin_service.users().list(
                customer='my_customer', domain=data['hd']).execute(http=http)
            auth_token = tokenutils.OAuth2TokenFromCredentials(
                decorator.credentials)
            emailsettings = EmailSettingsClient(domain=data['hd'])
            auth_token.authorize(emailsettings)
            signatureSchema = self.request.get("signature")

            if not self.request.get("ifDaily"):
                pass
            else:
                my_cron = SignatureCronTask()
                result = my_cron.find_signatures(data['email'])
                if result > 0:
                    my_cron.delete_signatures(data['email'])
                my_cron.create_signature(data, signatureSchema)

            for d in my_directory['users']:
                firstName = d['name']['givenName']
                lastName = d['name']['familyName']
                primaryEmail = d['primaryEmail']
                company = ''
                department = ''
                occupation = ''
                workPhone = ''
                homePhone = ''
                mobilePhone = ''
                workAddress = ''
                homeAddress = ''

                try:
                    if d['organizations']:
                        for o in d['organizations']:
                            occupation = o['title'] if o['title'] else ''
                            department = o['department'] if o[
                                'department'] else ''
                            company = o['name'] if o['name'] else ''
                except:
                    pass

                try:
                    if d['phones']:
                        for p in d['phones']:
                            if p['type'] == 'work':
                                workPhone = p['value']
                            elif p['type'] == 'mobile':
                                mobilePhone = p['value']
                            elif p['type'] == 'home':
                                homePhone = p['value']
                except:
                    pass

                try:
                    if d['addresses']:
                        for a in d['addresses']:
                            if a['type'] == 'work':
                                workAddress = a['formatted']
                            elif a['type'] == 'home':
                                homeAddress = a['formatted']
                except:
                    pass

                try:
                    signature = signatureSchema.format(
                        firstName=firstName, lastName=lastName, primaryEmail=primaryEmail,
                        company=company, occupation=occupation, department=department,
                        workPhone=workPhone, homePhone=homePhone, mobilePhone=mobilePhone,
                        workAddress=workAddress, homeAddress=homeAddress
                    )

                    emailsettings.UpdateSignature(
                        username=primaryEmail, signature=signature)
                except Exception, e:
                    my_mail.SendEmail(
                        to="crojas@azurian.com",
                        subject="Error al actualizar firma",
                        html=unicode(e.message)
                    )
                    raise e

            template = JINJA_ENVIRONMENT.get_template(
                "/signatures/signature_success.html")
            context = {
                'data': data,
            }
            self.response.write(template.render(context))
        except Exception, e:
            #self.response.write("Error, Authentication failure.")
            my_mail.SendEmail(
                to="crojas@azurian.com",
                subject="Error al actualizar firma",
                html=unicode(e.message)
            )
