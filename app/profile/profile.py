# encoding:utf-8
#!/usr/bin/env python

from oauth2client.client import AccessTokenRefreshError


import webapp2, os


from app.utils.oauthutils import decorator
from app.utils.oauthutils import user_info_service
from app.utils.oauthutils import plus_service
from app.utils.configs import JINJA_ENVIRONMENT
from app.utils import directory
from model.user_model import UserModel


class AuthControl(webapp2.RequestHandler):

    @decorator.oauth_aware
    def get(self):
        if not decorator.has_credentials():
            variables = {
                'url': decorator.authorize_url(),
                'has_credentials': decorator.has_credentials()
            }
            template = JINJA_ENVIRONMENT.get_template('index.html')
            self.response.write(template.render(variables))
        else:
            self.redirect('/profile')


class MyProfile(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            user = plus_service.people().get(
                userId='me').execute(http=http)
            token = decorator.credentials.access_token
            data = user_info_service.userinfo().get().execute(
                http=http)
            me_hd = None
            try:
                me_hd = directory.get_me_from_gal(
                    data['hd'], data['email'], token)
            except:
                pass

            user_model = UserModel()
            user_logged = user_model.find_user(data['email'])

            if user_logged == None:
                user_model.create_user(data, decorator.credentials)
            else:
                user_model.update_user(
                    user_logged, data, decorator.credentials)

            context = {
                'data': data,
                'user': user,
                'me_hd': me_hd,
            }
            template = JINJA_ENVIRONMENT.get_template('/profile/profile.html')
            self.response.write(template.render(context))
            #mipath = os.path.abspath('templates')
            #self.response.write(mipath)
        except AccessTokenRefreshError:
            self.redirect('/')


class RevokeOAuth2(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            token = decorator.credentials.access_token
            directory.revoke_access_token(token)
        finally:
            self.redirect("/")


class EditMyProfile(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            user = plus_service.people().get(userId='me').execute(http=http)
            data = user_info_service.userinfo().get().execute(http=http)
            context = {
                'data': data,
                'user': user,
            }
            template = JINJA_ENVIRONMENT.get_template(
                '/profile/edit_myprofile.html')
            self.response.write(template.render(context))
        except:
            self.redirect('/')
