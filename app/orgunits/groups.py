# encoding:utf-8
#!/usr/bin/env python

import json
import webapp2


from app.utils.oauthutils import decorator
from app.utils.oauthutils import user_info_service
from app.utils.oauthutils import admin_service
from app.utils.oauthutils import OAUTH_ERROR
from app.utils.configs import JINJA_ENVIRONMENT


class MyOrgUnitIndex(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        template = JINJA_ENVIRONMENT.get_template(
            "/orgunits/orgunits_index.html")
        self.response.write(template.render())


class MyOrgUnitMenu(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        http = decorator.http()
        data = user_info_service.userinfo().get().execute(http=http)
        template = JINJA_ENVIRONMENT.get_template("/orgunits/orgunits.html")
        context = {
            'data': data,
        }
        self.response.write(template.render(context))


class MyOrgUnitList(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            orgunits = admin_service.orgunits().list(
                customerId='my_customer', orgUnitPath='/', type='all').execute(http=http)
            template = JINJA_ENVIRONMENT.get_template(
                "/orgunits/orgunits_list.html")
            context = {
                'data': data,
                'orgunits': orgunits,
            }
            self.response.write(template.render(context))
        except:
            self.response.write(OAUTH_ERROR)


class CreateNewOrgUnit(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            orgunits = admin_service.orgunits().list(
                customerId='my_customer').execute(http=http)
            template = JINJA_ENVIRONMENT.get_template(
                "/orgunits/neworgunit.html")
            context = {
                'data': data,
                'orgunits': orgunits,
            }
            self.response.write(template.render(context))
        except:
            self.response.write(OAUTH_ERROR)

    @decorator.oauth_required
    def post(self):
        http = decorator.http()
        data = user_info_service.userinfo().get().execute(http=http)
        body_data = json.loads(self.request.body)
        name = body_data.get('name')
        description = body_data.get('description')
        parentOrgUnitPath = body_data.get('parentOrgUnitPath')
        body = {
            'name': name,
            'description': description,
            'parentOrgUnitPath': parentOrgUnitPath,
        }
        admin_service.orgunits().insert(
            customerId='my_customer', body=body).execute(http=http)
        self.response.write("OK")


class DeleteOrgUnit(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            orgunits = admin_service.orgunits().list(
                customerId='my_customer', orgUnitPath='/', type='all').execute(http=http)
            template = JINJA_ENVIRONMENT.get_template(
                "/orgunits/delorgunit.html")
            context = {
                'data': data,
                'orgunits': orgunits,
            }
            self.response.write(template.render(context))
        except:
            self.response.write(OAUTH_ERROR)

    @decorator.oauth_required
    def post(self):
        try:
            http = decorator.http()
            data = user_info_service.userinfo().get().execute(http=http)
            body_data = json.loads(self.request.body)
            orgUnitPath = body_data.get('orgUnitPath')
            orgUnitPath = orgUnitPath[1:]
            oauthutils.admin_service.orgunits().delete(
                customerId='my_customer', orgUnitPath=orgUnitPath).execute(http=http)
            self.redirect("/")
        except:
            self.response.write(OAUTH_ERROR)


class Test(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        pass


class MyGroupsList(webapp2.RequestHandler):

    @decorator.oauth_required
    def get(self):
        http = decorator.http()
        data = user_info_service.userinfo().get().execute(http=http)
        groupList = admin_service.groups().list().execute(http=http)
        self.response.write(groupList)
