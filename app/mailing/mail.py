# encoding:utf-8
#!/usr/bin/env python

import datetime


from google.appengine.api import mail


class MailingTool(object):

    """ Métodos de envío de correos de forma mas fácil """

    def SendEmail(self, to, subject, html):

        mail.send_mail(
            sender='crojas@azurian.com',
            to=to,
            subject=subject,
            body='',
            html=html,
        )
