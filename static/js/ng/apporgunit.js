var app = angular.module("orgUnits", 
	["ngRoute", "ui.bootstrap", "ngResource"]);
// manejador de botones laterales
app.controller('MenuController', function() {
	this.tab = 0;

	this.setTab = function(tab) {
		this.tab = tab;
	};

	this.isSet = function(tab) {
		return this.tab === tab;
	};
});

// manejador de rutas de /orgunits*
app.config(function($routeProvider) {
	$routeProvider
	.when("/index", {
		templateUrl: "/orgunits/index"
	})
	.when("/orgunitslist", {
		templateUrl: "/orgunits/list"
	})
	.when("/neworgunit", {
		templateUrl: '/orgunits/new',
		controller: FormOrgUnitController
	})
	.when("/delorgunit", {
		templateUrl: '/orgunits/del'
	})
	.otherwise({
		redirectTo: "/index"
	});
});

app.factory('OrgUnitResource', function($resource){
	var orgUnitslist = {
		getOrgUnits: $resource('/orgunits/listado', {}, {
			all: {method: 'GET', isArray: true}
		})
	};
	return orgUnitslist;
});

function DelModalController ($scope, $modal, $log, $http) {

	$scope.open = function (name) {
		alert(name);
		var modalInstance = $modal.open({
			templateUrl: '/orgunits/deletemodal',
			controller: DelModalInstanceController,
			resolve: {
				items: function () {
					return $scope.name;
				}
			}
		});

		modalInstance.result.then(function (selectedItem) {
			$scope.selected = selectedItem;
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
};

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.
function DelModalInstanceController ($scope, $modalInstance, items) {
	$scope.items = items;
	$scope.selected = {
	};

	$scope.ok = function () {
		$modalInstance.close($scope.selected.item);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
};

// manejador del formulario post de nueva orgunit
function FormOrgUnitController($scope, $http){
	$scope.addOrgUnit = function() {
		data = {name: $scope.org.name, 
				description: $scope.org.description,
				parentOrgUnitPath: $scope.org.parentOrgUnitPath};
		$http.post('/orgunits/new', data).success($scope.clearOrg());
	};

	$scope.clearOrg = function() {
		$scope.org.name = "";
		$scope.org.description = "";
		$scope.org.parentOrgUnitPath = "";
		$scope.org.orgUnitPath = "";
	};

	$scope.delOrgUnit = function() {

		data = {
			orgUnitPath: $scope.org.orgUnitPath
		};
		$http.post('/orgunits/del', data)
			.success(
				$scope.clearOrg()
			)
			.error(function() {
				alert("You cannot delete this organization because it has users");
		});
	};
};