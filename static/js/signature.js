$( document ).on( "ready", function () {

    $.ajax({
        type: "get",
        url: "http://console-io.appspot.com/emailtools/signature/query",
        //url: "http://localhost:8080/emailtools/signature/query",
        success: function ( data ) {
            var total = data;
            if ( total > 0 ) {
                $( "#deleteSignature" ).attr( "disabled" , false );
            };
        },
        error: function ( jqXHR, textSatus, errorThrown ) {
            console.assert( errorThrown );
        },
    });

    $( "#deleteSignature" ).on( "click", function () {
        $.ajax({
            type: "delete",
            url: "http://cloud-panel.appspot.com/emailtools/signature/query",
            //url: "http://localhost:8080/emailtools/signature/query",
            success: function ( data ) {
                var total = data;
                if ( total ) {
                    alert("The Cron Task has been removed");
                    $( "#deleteSignature" ).attr( "disabled" , true );
                };
            },
            error: function ( jqXHR, textSatus, errorThrown ) {
                console.assert( errorThrown );
            },
        });
    });

    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });

    $( "#firstName" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{firstName}");
    });

    $( "#lastName" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{lastName}");
    });

    $( "#email" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{primaryEmail}");
    });

    $( "#company" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{company}");
    });

    $( "#department" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{department}");
    });

    $( "#occupation" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{occupation}");
    });

    $( "#workAddress" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{workAddress}");
    });

    $( "#homeAddress" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{homeAddress}");
    });

    $( "#phone" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{phone}");
    });

    $( "#workPhone" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{workPhone}");
    });

    $( "#homePhone" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{homePhone}");
    });

    $( "#mobilePhone" ).on( "click", function() {
        tinyMCE.activeEditor.execCommand('mceInsertContent',0, "{mobilePhone}");
    });

    return false;
});