# encoding:utf-8
#!/usr/bin/env python

import datetime


from google.appengine.ext import ndb


from oauth2client.appengine import StorageByKeyName
from oauth2client.appengine import CredentialsModel


class UserModel(ndb.Model):

    name = ndb.StringProperty()
    email = ndb.StringProperty()
    picture = ndb.StringProperty()
    user_id = ndb.StringProperty()
    gender = ndb.StringProperty(default='None')
    refresh_token = ndb.StringProperty()
    signin_date = ndb.DateTimeProperty(auto_now_add=True)
    login_date = ndb.DateTimeProperty(auto_now=True)

    def find_user(self, email):

        return UserModel.query(UserModel.email == email).get()

    def create_user(self, data, credentials):

        self.name = data['name']
        self.email = data['email']
        try:
            self.picture = data['picture']
        except:
            pass
        try:
            self.gender = data['gender']
        except:
            pass
        self.user_id = data['id']
        self.refresh_token = credentials.refresh_token
        self.put()

        storage = StorageByKeyName(CredentialsModel, data['id'], 'credentials')
        credentials = credentials
        storage.locked_put(credentials)

    def update_user(self, user, data, credentials):

        user.name = data['name']
        try:
            user.picture = data['picture']
        except:
            pass
        try:
            user.gender = data['gender']
        except:
            pass
        user.user_id = data['id']
        user.refresh_token = credentials.refresh_token
        user.login_date = datetime.datetime.now()
        user.put()

        storage = StorageByKeyName(CredentialsModel, data['id'], 'credentials')
        credentials = credentials
        storage.locked_put(credentials)

    def get_credential(self, id):

        return StorageByKeyName(CredentialsModel, id, 'credentials').get()