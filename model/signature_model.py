# encoding:utf-8
#!/usr/bin/env python

import datetime


from google.appengine.ext import ndb


class SignatureCronTask(ndb.Model):

    user_id = ndb.StringProperty()
    email = ndb.StringProperty()
    domain = ndb.StringProperty(default='gmail.com')
    signature = ndb.TextProperty()

    def create_signature(self, data, signature):

        self.user_id = data['id']
        self.email = data['email']
        self.domain = data['hd']
        self.signature = signature
        self.put()

    def list_signatures(self):

        return SignatureCronTask.query().fetch()

    def find_signatures(self, email):

        return SignatureCronTask.query(SignatureCronTask.email == email).fetch()

    def delete_signatures(self, email):

    	result = SignatureCronTask.query(SignatureCronTask.email == email).fetch()

    	for res in result:
    		res.key.delete()